/*
 * libjmap
 *
 * Copyright 2019 Manos Pitsidianakis
 *
 * This file is part of libjmap.
 *
 * libjmap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libjmap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libjmap. If not, see <http://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate smallvec;
use smallvec::SmallVec;

#[macro_export]
macro_rules! _impl {
        ($(#[$outer:meta])*$field:ident : $t:ty) => {
            $(#[$outer])*
            pub fn $field(mut self, new_val: $t) -> Self {
                self.$field = new_val;
                self
            }
        };
        (get_mut $(#[$outer:meta])*$method:ident, $field:ident : $t:ty) => {
            $(#[$outer])*
            pub fn $method(&mut self) -> &mut $t {
                &mut self.$field
            }
        };
        (get $(#[$outer:meta])*$method:ident, $field:ident : $t:ty) => {
            $(#[$outer])*
            pub fn $method(&self) -> &$t {
                &self.$field
            }
        }
    }

use serde_derive::{Deserialize, Serialize};
pub mod objects;
pub mod protocol;
pub mod rfc8620;

use rfc8620::*;
use std::sync::{Arc, Mutex};

pub(crate) fn bytes_find<T: AsRef<[u8]>>(bytes: &[u8], needle: T) -> Option<usize> {
    let needle = needle.as_ref();
    if needle.is_empty() {
        return None;
    }
    bytes
        .windows(needle.len())
        .position(|window| window == needle)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
